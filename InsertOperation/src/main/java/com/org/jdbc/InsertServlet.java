package com.org.jdbc;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class InsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Insert p=null;  
    InsertDao prod=new InsertDao();
    Scanner s=new Scanner(System.in);
    public InsertServlet() {
        super();
         
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       
		String action=request.getServletPath();
		switch(action) {
		case "/add":insert(request,response);
		            break;
		            
		case "/show":try {
				list(request,response);
			} catch (Exception e) {
				e.printStackTrace();
			}
		        break;
		        
		case "/addById":try {
			insertById(request,response);
		} catch (Exception e) {
			e.printStackTrace();
		}
            break;
            
	    case "/edit":
	    	try {
				update(request,response);
			} catch (Exception e) {
				e.printStackTrace();
			}
	    	break;
	    	
	    case "/delete":
	    	try {
				delete(request,response);
			} catch (Exception e) {
				e.printStackTrace();
			}
	    	break;
	} 
  }
   private void delete(HttpServletRequest request, HttpServletResponse response) throws Exception {
	System.out.println("Enter id");
	   int id=s.nextInt();
	   prod.deleteProduct(id);
	   System.out.println("Deleted");		
	}



	
	private void update(HttpServletRequest request, HttpServletResponse response) throws Exception {
		   System.out.println("Enter id");
	       int id=s.nextInt();
	       p=prod.getProductById(id);
	       System.out.println(p);
	       System.out.println("Enter the price to be updated");
	       int newPrice=s.nextInt();
	       p.setProduct_Price(newPrice);
	       prod.updateProduct(p);
	       System.out.println("Updated");		
	}


	private void insertById(HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println("Enter id");
        int id=s.nextInt();
        p=prod.getProductById(id);
        System.out.println(p. getProduct_Name()+" "+p.getProductID()+" "+p.getProduct_Description()+" "+p.getProduct_Price()+" "+p.getProduct_Quantity()+" "+p.getPass());
	}


	private void list(HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<Insert> list=prod.getProduct();
		for(Insert i:list) {
			System.out.println(i);
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	void insert(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		 System.out.println("Enter product name");
		   String Product_Name=s.next();
		   System.out.println("Enter product id");
	       int ProductID=s.nextInt();
	       System.out.println("Enter product desc");
	       String Product_Description=s.next();
	       System.out.println("Enter product price");
	       int Product_Price=s.nextInt();
	       System.out.println("Enter product quantity");
	       int Product_Quantity=s.nextInt();
	       System.out.println("Enter product pass");
	       String Pass=s.next();
	       p=new Insert(Product_Name,ProductID,Product_Description,Product_Price,Product_Quantity,Pass);
		   try {
			prod.insert(p);
		} catch (Exception e) {

			e.printStackTrace();
		}
		   PrintWriter pw=response.getWriter();
		   pw.println("Row Updated");
	}

}
