package com.org.jdbc;

public class Insert {
    
	 private String Product_Name;
	 private int ProductID;
	 private String Product_Description;
	 private int Product_Price;
	 private int Product_Quantity;
	 private String Pass;
	 
	 public Insert() {}
	public Insert(String product_Name, int productID, String product_Description, int product_Price,
			int product_Quantity,String pass) {
		super();
		Product_Name = product_Name;
		ProductID = productID;
		Product_Description = product_Description;
		Product_Price = product_Price;
		Product_Quantity = product_Quantity;
		Pass=pass;
	}
	
	
	

	public String getProduct_Name() {
		return Product_Name;
	}
	public int getProductID() {
		return ProductID;
	}
	public String getProduct_Description() {
		return Product_Description;
	}
	public int getProduct_Price() {
		return Product_Price;
	}
	public int getProduct_Quantity() {
		return Product_Quantity;
	}
	public String getPass() {
		return Pass;
	}
	public void setProduct_Name(String product_Name) {
		Product_Name = product_Name;
	}
	public void setProductID(int productID) {
		ProductID = productID;
	}
	public void setProduct_Description(String product_Description) {
		Product_Description = product_Description;
	}
	public void setProduct_Price(int product_Price) {
		Product_Price = product_Price;
	}
	public void setProduct_Quantity(int product_Quantity) {
		Product_Quantity = product_Quantity;
	}
	public void setPass(String pass) {
		Pass = pass;
	}
	@Override
	public String toString() {
		return Product_Name+" "+ ProductID+" "+ Product_Description+" "+ Product_Price+" "+ Product_Quantity;
	}
	
}
