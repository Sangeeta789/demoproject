package com.org.jdbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.org.jdbc.Insert;


public class InsertDao {
	ArrayList<Insert> prodlist=new ArrayList<Insert>();
	Insert p;
	  public static Connection getConnection() throws Exception{
	  
	    Class.forName("org.mariadb.jdbc.Driver");
	    String url="jdbc:mariadb://localhost:3306/PRODUCT";
	    String user="root";
	    String password="root";
	    Connection con=DriverManager.getConnection(url,user,password);
	    return con;
	  }
	  
	  boolean insert(Insert p) throws Exception{
		  Connection conn=getConnection();
		  String q="INSERT INTO Pdetails values(?,?,?,?,?,?)";
		  PreparedStatement ps=conn.prepareStatement(q);
		  ps.setString(1,p.getProduct_Name());
		  ps.setInt(2,p.getProductID());
		  ps.setString(3,p.getProduct_Description());
		  ps.setInt(4, p.getProduct_Price());
		  ps.setInt(5,p.getProduct_Quantity());
		  ps.setString(6,p.getPass());
		  int i=ps.executeUpdate();
		  System.out.println("Updated");
		  if(i==1)
			  return true;
		  return false;
		  
	  }
	  
	  public List<Insert> getProduct() throws Exception{
		   Connection conn=getConnection();
		   Statement st=conn.createStatement();
		   String q="Select * from Pdetails";
		   ResultSet rs=st.executeQuery(q);
		   while(rs.next()) {
			   p=new Insert(rs.getString(1),rs.getInt(2),rs.getString(3),rs.getInt(4),rs.getInt(5),rs.getString(6));
			   prodlist.add(p);
		   }
		   return prodlist;
	   }
	  
	  public Insert getProductById(int id) throws Exception{
		   Connection conn=getConnection();
		   Statement st=conn.createStatement();	  
		   String q="Select * from Pdetails where ProductID = "+ id +" ";
		   ResultSet rs=st.executeQuery(q);
		   rs.next();
		   p=new Insert(rs.getString(1),rs.getInt(2),rs.getString(3),rs.getInt(4),rs.getInt(5),rs.getString(6));
		   return p;
	   }
	  
	  boolean updateProduct(Insert p)throws Exception {
		  Connection conn=getConnection();
		  String q="Update Pdetails set Product_Price=? where ProductID=?";
		  PreparedStatement ps=conn.prepareStatement(q);
		  ps.setInt(1,p.getProduct_Price());
		  ps.setInt(2, p.getProductID());
		  int i=ps.executeUpdate();
		  ps.close();
		  conn.close();
		  if(i==1) 
			  return true;
		  
		  return false;
	  }
	  
	  boolean deleteProduct(int id) throws Exception{
		   Connection conn=getConnection();
		   String q="Delete from Pdetails where ProductID=?";
		   PreparedStatement ps=conn.prepareStatement(q);
		   ps.setInt(1, id);
		   int i=ps.executeUpdate();
		   ps.close();
		   conn.close();
		   if(i==1)
			   return true;
		   return false;
		   
	   }
	  
}
